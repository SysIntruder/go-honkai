# Go-Honkai
> Honkai impact 3rd damage calculator written with go

## Project Structure
```
.env
go.mod
main.go
|-  config
|   |-  config.go                   #   Read and init config
|-  src
|   |-  helper                      #   Helper functions
|   |-  modules                     #   Modules
|   |   |-  <module>
|   |   |-  |-  *_action.go         #   Use case
|   |   |-  |-  *_api.go            #   Api path
|   |   |-  |-  *_entity.go         #   Entity
|   |   |-  |-  *_handler.go        #   Request handler
|   |   |-  |-  *_provider.go       #   Data provider
|   |   |-  |-  *_test.go           #   Test file
|   |-  server
|   |   |-  api.go                  #   Init API
|   |   |-  server.go               #   Init server
```

## Module Testing
```
go test go-honkai/src/modules/<module> -v
```