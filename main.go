package main

import (
	"go-honkai/config"
	"go-honkai/src/server"
)

func main() {
	config.InitConfig()

	server.InitServer()
}
