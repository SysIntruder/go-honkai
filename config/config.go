package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type (
	Env struct {
		HttpPort	string
	}

	DbConf struct {
		User		string
		Pass		string
		Host		string
		Port		string
		Name		string
	}
)

var GlobalEnv Env

func InitConfig() {
	if err := godotenv.Load(); err != nil {
		log.Print(`Can't find .env`)
		panic(`.env not found`)
	}

	var ok bool

	GlobalEnv.HttpPort, ok = os.LookupEnv(`HTTP_PORT`)
	if !ok {
		panic(`HTTP_PORT .env must be set!`)
	}
}
