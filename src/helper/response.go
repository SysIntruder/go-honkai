package helper

import (
	"net/http"
)

type ResParams struct {
	Status int
	Data   []byte
	Writer http.ResponseWriter
}

func SendResponse(params ResParams) {
	params.Writer.WriteHeader(params.Status)
	params.Writer.Write(params.Data)
}
