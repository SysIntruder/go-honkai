package server

import (
	"net/http"

	"go-honkai/src/modules/damage"
	"go-honkai/src/modules/valkyrie"

	"github.com/go-chi/chi/v5"
)

func initApi(api chi.Router) {
	api.Route(`/api`, func(router chi.Router) {
		router.Get(`/`, func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte(`GET /api`))
		})

		valkyrie.InitValkApi(router)
		damage.InitDamageApi(router)
	})
}
