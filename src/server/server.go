package server

import (
	"net/http"

	"go-honkai/config"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func InitServer() {
	api := chi.NewRouter()
	api.Use(middleware.Recoverer)

	api.Get(`/`, func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`May all the beauty be blessed~`))
	})

	initApi(api)

	http.ListenAndServe(config.GlobalEnv.HttpPort, api)
}
