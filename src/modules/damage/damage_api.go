package damage

import (
	"encoding/json"
	"net/http"

	"go-honkai/src/helper"

	"github.com/go-chi/chi/v5"
)

type (
	reqDamage struct {
		Damage      float64 `json:"damage"`
		TotalDamage float64 `json:"total_damage"`
	}

	reqGetDamage struct {
		ID           int       `json:"valk_id"`
		AtkMod       float64   `json:"valk_atk_mod"`
		Damage       reqDamage `json:"valk_damage"`
		Critical     float64   `json:"valk_critical"`
		Stage        reqDamage `json:"valk_stage"`
		TargetAttr   int       `json:"valk_target_attr"`
		TargetDebuff reqDamage `json:"valk_target_debuff"`
		TargetVuln   reqDamage `json:"valk_target_vuln"`
	}
)

func InitDamageApi(api chi.Router) {
	api.Route(`/damage`, func(router chi.Router) {
		router.Post(`/`, func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set(`Content-Type`, `application/json`)
			resp := helper.ResParams{
				Status: http.StatusOK,
				Writer: w,
			}

			var req reqGetDamage

			decoder := json.NewDecoder(r.Body)
			decoder.DisallowUnknownFields()

			if err := decoder.Decode(&req); err != nil {
				resp.Status = http.StatusInternalServerError
				resp.Data = []byte(`{}`)

				helper.SendResponse(resp)
			}

			res, err := handleGetDamage(req)
			if err != nil {
				resp.Status = http.StatusInternalServerError
				resp.Data = []byte(`{}`)

				helper.SendResponse(resp)
			}

			resp.Data = res
			helper.SendResponse(resp)
		})
	})
}
