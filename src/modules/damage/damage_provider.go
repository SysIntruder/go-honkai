package damage

type (
	DamageProvider struct{}
)

func (dp DamageProvider) GetDmgTypeList() *map[int]DamageType {
	return &DmgTypeList
}

func (dp DamageProvider) GetDmgType(id int) *DamageType {
	for _, item := range DmgTypeList {
		if item.ID == id {
			return &item
		}
	}

	return nil
}

func InitDamageProvider() DamageProvider {
	return DamageProvider{}
}

var DmgTypeList = map[int]DamageType{
	0: {ID: 1, Name: `Physical`},
	1: {ID: 2, Name: `Elemental`},
	2: {ID: 3, Name: `All`},
}
