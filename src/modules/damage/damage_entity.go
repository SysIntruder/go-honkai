package damage

type (
	DamageType struct {
		ID		int
		Name	string
	}

	Damage struct {
		Type	int
		Value	float64
	}

	FinalDamage struct {
		Type	int
		Value	float64
		Crit	float64
	}
)
