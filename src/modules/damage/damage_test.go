package damage

import (
	"reflect"
	"testing"

	"go-honkai/src/modules/valkyrie"
)

type mockProvider struct{}

func (md mockProvider) GetValkyrie(id int) *valkyrie.Valkyrie {
	return &valkyrie.Valkyrie{ID: 1, Name: `Herrscer of Void`, AttrID: 1, DamageTypeID: 1, ATK: 1550, CRT: 129}
}

func (md mockProvider) GetAttribute(id int) *valkyrie.Attribute {
	return &valkyrie.Attribute{ID: 1, Name: `Bio`, Value: 0.3, UpTo: 2, DownTo: 3}
}

func testInitDamageCalculator(t *testing.T) {
	dc := DamageCalculator{}
	ac := InitDamageCalculator()

	if reflect.TypeOf(ac) != reflect.TypeOf(dc) {
		t.Fatal(`InitDamageCalculator() result must be DamageCalculator`)
	}
}

func testBaseAtk(t *testing.T) {
	params := baseAtkParams{
		ID:     1,
		AtkMod: 2.5,
	}
	dp := mockProvider{}
	want := 3875.0
	ch := make(chan Damage)
	go InitDamageCalculator().BaseAtk(params, dp, ch)
	damage := <-ch

	if damage.Value != want {
		t.Fatalf(`BaseAtk(params).Value = %f, must be %#f`, damage.Value, want)
	}
}

func testCritRate(t *testing.T) {
	params := critRateParams{
		Crt:   180,
		Level: 80,
	}
	want := 37.89
	ch := make(chan float64)
	go InitDamageCalculator().CritRate(params, ch)
	critRate := <-ch

	if critRate != want {
		t.Fatalf(`CrtRate(params) = %f, must be %#f`, critRate, want)
	}
}

func testCritMod(t *testing.T) {
	want := 2.63
	ch := make(chan float64)
	go InitDamageCalculator().CritMod(0.63, ch)
	critMod := <-ch

	if critMod != want {
		t.Fatalf(`CrtMod(bonus) = %f, must be %#f`, critMod, want)
	}
}

func testAttrMod(t *testing.T) {
	params := attrModParams{
		ID:         1,
		TargetAttr: 3,
	}
	dp := mockProvider{}
	want := 0.7
	ch := make(chan float64)
	go InitDamageCalculator().AttrMod(params, dp, ch)
	attrMod := <-ch

	if attrMod != want {
		t.Fatalf(`AttrMod(mod) = %f, must be %#f`, attrMod, want)
	}
}

func testDmgMod(t *testing.T) {
	params := dmgModParams{
		ID:  1,
		Dmg: 2.5,
		Tdm: 3.34,
	}
	dp := mockProvider{}
	want := 15.19
	ch := make(chan Damage)
	go InitDamageCalculator().DmgMod(params, dp, ch)
	damage := <-ch

	if damage.Value != want {
		t.Fatalf(`DmgMod(params).Value = %f, must be %#f`, damage.Value, want)
	}
}

func testFinalDmg(t *testing.T) {
	params := finalDmgParams{
		ID:     1,
		Base:   5425,
		Char:   15.19,
		Crit:   2.5,
		Attr:   1.3,
		Vuln:   1.5,
		Debuff: 3.5,
		Stage:  2.25,
	}
	dp := mockProvider{}
	want := 1265443.30
	ch := make(chan FinalDamage)
	go InitDamageCalculator().FinalDmg(params, dp, ch)
	finalDamage := <-ch

	if finalDamage.Crit == 0 {
		t.Fatalf(`FinalDmg(params).Crit = %f, must not 0`, finalDamage.Crit)
	}

	if finalDamage.Value != want {
		t.Fatalf(`FinalDmg(params).Value = %f, must be %#f`, finalDamage.Value, want)
	}
}

func testInitDamageProvider(t *testing.T) {
	dp := InitDamageProvider()
	pr := DamageProvider{}

	if reflect.TypeOf(dp) != reflect.TypeOf(pr) {
		t.Fatal(`InitDamageProvider() result must be DamageProvider`)
	}
}

func testGetDmgTypeList(t *testing.T) {
	dp := InitDamageProvider()
	dmgList := &map[int]DamageType{}
	res := dp.GetDmgTypeList()

	if reflect.TypeOf(res) != reflect.TypeOf(dmgList) {
		t.Fatal(`GetDmgTypeList() result must be *map[int]DamageType`)
	}
}

func testGetDmgType(t *testing.T) {
	dp := InitDamageProvider()
	dmg := &DamageType{}
	res := dp.GetDmgType(1)

	if reflect.TypeOf(res) != reflect.TypeOf(dmg) {
		t.Fatal(`GetDmgType(id) result must be *DamageType`)
	}
}

func TestDamage(t *testing.T) {
	t.Run(`Test Damage Action`, func(t *testing.T) {
		t.Run(`InitDamageCalculator()`, testInitDamageCalculator)
		t.Run(`BaseAtk(params,provider,channel)`, testBaseAtk)
		t.Run(`CritRate(params,channel)`, testCritRate)
		t.Run(`CritMod(params,channel)`, testCritMod)
		t.Run(`AttrMod(params,provider,channel)`, testAttrMod)
		t.Run(`DmgMod(params,provider,channel)`, testDmgMod)
		t.Run(`FinalDmg(params,provider,channel)`, testFinalDmg)
	})
	t.Run(`Test Damage Provider`, func(t *testing.T) {
		t.Run(`InitDamageProvider()`, testInitDamageProvider)
		t.Run(`GetDmgTypeList()`, testGetDmgTypeList)
		t.Run(`GetDmgType(id)`, testGetDmgType)
	})
}
