package damage

import (
	"math"

	"go-honkai/src/modules/valkyrie"
)

type (
	DamageCalculator struct{}

	AttrProviderInterface interface {
		GetAttribute(id int) *valkyrie.Attribute
		GetValkyrie(id int) *valkyrie.Valkyrie
	}

	ValkProviderInterface interface {
		GetValkyrie(id int) *valkyrie.Valkyrie
	}

	baseAtkParams struct {
		ID     int
		AtkMod float64
	}

	critRateParams struct {
		Crt   float64
		Level float64
	}

	dmgModParams struct {
		ID  int
		Dmg float64
		Tdm float64
	}

	attrModParams struct {
		ID         int
		TargetAttr int
	}

	finalDmgParams struct {
		ID     int
		Base   float64
		Char   float64
		Crit   float64
		Attr   float64
		Vuln   float64
		Debuff float64
		Stage  float64
	}
)

func (dc DamageCalculator) BaseAtk(params baseAtkParams, pr ValkProviderInterface, ch chan Damage) {
	res := pr.GetValkyrie(params.ID)
	if res == nil {
		panic(`Can't find valkyrie`)
	}
	valk := *res

	value := math.Round((valk.ATK*params.AtkMod)*100) / 100

	ch <- Damage{
		Type:  valk.DamageTypeID,
		Value: value,
	}

	defer close(ch)
}

func (dc DamageCalculator) CritRate(params critRateParams, ch chan float64) {
	crMultiplier := 5.0
	crAddition := 75.0
	critRate := math.Round((params.Crt/(params.Level*crMultiplier+crAddition)*100)*100) / 100

	ch <- critRate

	defer close(ch)
}

func (dc DamageCalculator) CritMod(bonus float64, ch chan float64) {
	crit := math.Round((1+1+bonus)*100) / 100

	ch <- crit

	defer close(ch)
}

func (dc DamageCalculator) AttrMod(params attrModParams, pr AttrProviderInterface, ch chan float64) {
	resValk := pr.GetValkyrie(params.ID)
	if resValk == nil {
		panic(`Can't find valkyrie`)
	}
	valk := *resValk

	resAttr := pr.GetAttribute(valk.AttrID)
	if resAttr == nil {
		panic(`Can't find attribute`)
	}
	attr := *resAttr

	var mod float64
	if attr.UpTo == params.TargetAttr {
		mod = attr.Value
	} else if attr.DownTo == params.TargetAttr {
		mod = math.Copysign(attr.Value, -1)
	}

	attrMod := math.Round((1+mod)*100) / 100

	ch <- attrMod

	defer close(ch)
}

func (dc DamageCalculator) DmgMod(params dmgModParams, pr ValkProviderInterface, ch chan Damage) {
	resValk := pr.GetValkyrie(params.ID)
	if resValk == nil {
		panic(`Can't find valkyrie`)
	}
	valk := *resValk

	value := math.Round(((1+params.Tdm)*(1+params.Dmg))*100) / 100

	ch <- Damage{
		Type:  valk.DamageTypeID,
		Value: value,
	}

	defer close(ch)
}

func (dc DamageCalculator) FinalDmg(params finalDmgParams, pr ValkProviderInterface, ch chan FinalDamage) {
	resValk := pr.GetValkyrie(params.ID)
	if resValk == nil {
		panic(`Can't find valkyrie`)
	}
	valk := *resValk

	value := math.Round((params.Base*params.Char*params.Attr*params.Vuln*params.Debuff*params.Stage)*100) / 100

	var crit, critical float64 = 1.0, 0
	if valk.DamageTypeID == 1 {
		crit = params.Crit
		critical = math.Round((value*crit)*100) / 100
	}

	ch <- FinalDamage{
		Type:  valk.DamageTypeID,
		Value: value,
		Crit:  critical,
	}

	defer close(ch)
}

func InitDamageCalculator() DamageCalculator {
	return DamageCalculator{}
}
