package damage

import (
	"encoding/json"

	"go-honkai/src/modules/valkyrie"
)

type resGetDamage struct {
	Valkyrie    valkyrie.Valkyrie `json:"valkyrie"`
	DamageType  DamageType        `json:"damage_type"`
	FinalDamage FinalDamage       `json:"final_damage"`
}

func handleGetDamage(req reqGetDamage) ([]byte, error) {
	dc := InitDamageCalculator()
	dp := InitDamageProvider()
	vp := valkyrie.InitValkProvider()

	valkyrie := vp.GetValkyrie(req.ID)

	damageType := dp.GetDmgType(valkyrie.DamageTypeID)

	baseCh := make(chan Damage)
	go dc.BaseAtk(baseAtkParams{
		ID:     req.ID,
		AtkMod: req.AtkMod,
	}, vp, baseCh)

	charCh := make(chan Damage)
	go dc.DmgMod(dmgModParams{
		ID:  req.ID,
		Dmg: req.Damage.Damage,
		Tdm: req.Damage.TotalDamage,
	}, vp, charCh)

	critCh := make(chan float64)
	go dc.CritMod(req.Critical, critCh)

	vulnCh := make(chan Damage)
	go dc.DmgMod(dmgModParams{
		ID:  req.ID,
		Dmg: req.TargetVuln.Damage,
		Tdm: req.TargetVuln.TotalDamage,
	}, vp, vulnCh)

	debuffCh := make(chan Damage)
	go dc.DmgMod(dmgModParams{
		ID:  req.ID,
		Dmg: req.TargetDebuff.Damage,
		Tdm: req.TargetDebuff.TotalDamage,
	}, vp, debuffCh)

	stageCh := make(chan Damage)
	go dc.DmgMod(dmgModParams{
		ID:  req.ID,
		Dmg: req.Stage.Damage,
		Tdm: req.Stage.TotalDamage,
	}, vp, stageCh)

	attrCh := make(chan float64)
	go dc.AttrMod(attrModParams{
		ID:         req.ID,
		TargetAttr: req.TargetAttr,
	}, vp, attrCh)

	baseAtk := <-baseCh
	charMod := <-charCh
	critMod := <-critCh
	vulnMod := <-vulnCh
	debuffMod := <-debuffCh
	stageMod := <-stageCh
	attrmod := <-attrCh

	finalCh := make(chan FinalDamage)
	go dc.FinalDmg(finalDmgParams{
		ID:     req.ID,
		Base:   baseAtk.Value,
		Char:   charMod.Value,
		Crit:   critMod,
		Attr:   attrmod,
		Vuln:   vulnMod.Value,
		Debuff: debuffMod.Value,
		Stage:  stageMod.Value,
	}, vp, finalCh)
	finalDamage := <-finalCh

	result := resGetDamage{
		Valkyrie:    *valkyrie,
		DamageType:  *damageType,
		FinalDamage: finalDamage,
	}

	res, err := json.Marshal(result)
	if err != nil {
		return nil, err
	}

	return res, nil
}
