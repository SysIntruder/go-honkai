package valkyrie

import (
	"reflect"
	"testing"
)

func testInitValkProvider(t *testing.T) {
	vp := InitValkProvider()
	pr := ValkyrieProvider{}

	if reflect.TypeOf(vp) != reflect.TypeOf(pr) {
		t.Fatal(`InitValkProvider() result must be ValkyrieProvider`)
	}
}

func testGetValkList(t *testing.T) {
	vp := InitValkProvider()
	valkList := &map[int]Valkyrie{}
	res := vp.GetValkList()

	if reflect.TypeOf(res) != reflect.TypeOf(valkList) {
		t.Fatal(`GetValkList() result must be *map[int]Valkyrie`)
	}
}

func testGetAttrList(t *testing.T) {
	vp := InitValkProvider()
	attrList := &map[int]Attribute{}
	res := vp.GetAttrList()

	if reflect.TypeOf(res) != reflect.TypeOf(attrList) {
		t.Fatal(`GetAttrList() result must be *map[int]Attribute`)
	}
}

func testGetValkyrie(t *testing.T) {
	vp := InitValkProvider()
	valk := &Valkyrie{}
	res := vp.GetValkyrie(1)

	if reflect.TypeOf(res) != reflect.TypeOf(valk) {
		t.Fatal(`GetValkyrie(id) result must be *Valkyrie`)
	}
}

func testGetAttribute(t *testing.T) {
	vp := InitValkProvider()
	attr := &Attribute{}
	res := vp.GetAttribute(1)

	if reflect.TypeOf(res) != reflect.TypeOf(attr) {
		t.Fatal(`GetAttribute(id) result must be *Attribute`)
	}
}

func TestValkyrie(t *testing.T) {
	t.Run(`Test Valkyrie Provider`, func(t *testing.T) {
		t.Run(`InitValkProvider()`, testInitValkProvider)
		t.Run(`GetValkList()`, testGetValkList)
		t.Run(`GetAttrList()`, testGetAttrList)
		t.Run(`GetValkyrie()`, testGetValkyrie)
		t.Run(`GetAttribute()`, testGetAttribute)
	})
}
