package valkyrie

import (
	"encoding/json"
	"errors"
)

func handleGetValkyrie() ([]byte, error) {
	vp := InitValkProvider()
	list := vp.GetValkList()
	if list == nil {
		return nil, errors.New(`error querying result`)
	}

	valkList := *list
	res, err := json.Marshal(valkList)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func handleGetAttribute() ([]byte, error) {
	vp := InitValkProvider()
	list := vp.GetAttrList()
	if list == nil {
		return nil, errors.New(`error querying result`)
	}

	attrList := *list
	res, err := json.Marshal(attrList)
	if err != nil {
		return nil, err
	}

	return res, nil
}
