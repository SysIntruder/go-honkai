package valkyrie

type (
	Valkyrie struct {
		ID           int
		Name         string
		AttrID       int
		DamageTypeID int
		ATK          float64
		CRT          float64
	}

	Attribute struct {
		ID     int
		Name   string
		Value  float64
		UpTo   int
		DownTo int
	}
)
