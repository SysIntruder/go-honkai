package valkyrie

import (
	"net/http"

	"go-honkai/src/helper"

	"github.com/go-chi/chi/v5"
)

func InitValkApi(api chi.Router) {
	api.Route(`/valkyrie`, func(router chi.Router) {
		router.Get(`/`, func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set(`Content-Type`, `application/json`)
			resp := helper.ResParams{
				Status: http.StatusOK,
				Writer: w,
			}

			res, err := handleGetValkyrie()
			if err != nil {
				resp.Status = http.StatusInternalServerError
				resp.Data = []byte(`{}`)

				helper.SendResponse(resp)
			}

			resp.Data = res
			helper.SendResponse(resp)
		})
		router.Get(`/attr`, func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set(`Content-Type`, `application/json`)
			resp := helper.ResParams{
				Status: http.StatusOK,
				Writer: w,
			}

			res, err := handleGetAttribute()
			if err != nil {
				resp.Status = http.StatusInternalServerError
				resp.Data = []byte(`{}`)

				helper.SendResponse(resp)
			}

			resp.Data = res
			helper.SendResponse(resp)
		})
	})
}
