package valkyrie

type ValkyrieProvider struct{}

func (vp ValkyrieProvider) GetValkList() *map[int]Valkyrie {
	return &ValkList
}

func (vp ValkyrieProvider) GetAttrList() *map[int]Attribute {
	return &AttrList
}

func (vp ValkyrieProvider) GetAttribute(id int) *Attribute {
	for _, item := range AttrList {
		if item.ID == id {
			return &item
		}
	}

	return nil
}

func (vp ValkyrieProvider) GetValkyrie(id int) *Valkyrie {
	for _, item := range ValkList {
		if item.ID == id {
			return &item
		}
	}

	return nil
}

func InitValkProvider() ValkyrieProvider {
	return ValkyrieProvider{}
}

var ValkList = map[int]Valkyrie{
	0: {
		ID:           1,
		Name:         `Herrscher of Void`,
		AttrID:       1,
		DamageTypeID: 1,
		ATK:          1550,
		CRT:          149,
	},
	1: {
		ID:           2,
		Name:         `Herrscher of Reason`,
		AttrID:       3,
		DamageTypeID: 2,
		ATK:          1539,
		CRT:          129,
	},
	2: {
		ID:           3,
		Name:         `Stygian Nymph`,
		AttrID:       4,
		DamageTypeID: 1,
		ATK:          1524,
		CRT:          155,
	},
	3: {
		ID:           4,
		Name:         `Herrscher of Thunder`,
		AttrID:       2,
		DamageTypeID: 2,
		ATK:          1597,
		CRT:          96,
	},
	4: {
		ID:           5,
		Name:         `Herrscher of Sentience`,
		AttrID:       1,
		DamageTypeID: 1,
		ATK:          1513,
		CRT:          180,
	},
	5: {
		ID:           6,
		Name:         `herrscher of Flamescion`,
		AttrID:       2,
		DamageTypeID: 2,
		ATK:          1621,
		CRT:          99,
	},
}

var AttrList = map[int]Attribute{
	0: {ID: 1, Name: `Bio`, Value: 0.3, UpTo: 2, DownTo: 3},
	1: {ID: 2, Name: `Psy`, Value: 0.3, UpTo: 3, DownTo: 1},
	2: {ID: 3, Name: `Mech`, Value: 0.3, UpTo: 1, DownTo: 2},
	3: {ID: 4, Name: `Qua`, Value: 0.3, UpTo: 4, DownTo: 5},
	4: {ID: 5, Name: `Img`, Value: 0.3, UpTo: 5, DownTo: 4},
}
